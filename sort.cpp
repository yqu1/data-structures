#include "sort.h" 

int main()
{
    int temparr[9] = {0,5,2,4,6,7,3,6,5};
    vector<int> vec (temparr, temparr + sizeof(temparr) / sizeof(int) );
    vector<int> temp = vec;

    cout << "we sort the following array: " << endl;
    for(int i = 0; i < 9; i++) {
        cout << temp[i] << " ";
    }
    cout << endl;

    cout << "mergesort..." << endl;
    mergesort(temparr, 0, 8);

    cout << "result: " << endl;

    for(int i = 0; i < 9; i++) {
        cout << temparr[i] << " ";
    }
    cout << endl;


    cout<<"bucket sort: "<<endl;
    bucket(temp,20);
    cout << "result: " << endl;
    print(temp);

    temp = vec;

    cout<<"shell sort: "<<endl;
    shellsort(temp);
    cout << "result: " << endl;
    print(temp);

    temp = vec;

    cout<<"quick sort: "<<endl;
    quicksort(temp, 0, 8);
    cout << "result: " << endl;
    print(temp);

    temp = vec;


    return 0;
}