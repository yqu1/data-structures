#include <vector>
#include <iostream>
#include "Darray.h"
using namespace std;

int main()
{
	int A[]={1,2,3,4,5,6,7};
	Darray a(A, 7);
	Darray b(7);
	b = a;
	b.print();
	cout << (b == a) << endl;
	cout << b[3] << endl;
}