#include "graph.h"

int main()
{


	Wgraph wgraph(9);
	MST mst(9);


  cout << "demonstration of dijkstra: " << endl;

	int graph[9][9] = {{0, 4, 0, 0, 0, 0, 0, 8, 0},
                      {4, 0, 8, 0, 0, 0, 0, 11, 0},
                      {0, 8, 0, 7, 0, 4, 0, 0, 2},
                      {0, 0, 7, 0, 9, 14, 0, 0, 0},
                      {0, 0, 0, 9, 0, 10, 0, 0, 0},
                      {0, 0, 4, 0, 10, 0, 2, 0, 0},
                      {0, 0, 0, 14, 0, 2, 0, 1, 6},
                      {8, 11, 0, 0, 0, 0, 1, 0, 7},
                      {0, 0, 2, 0, 0, 0, 6, 7, 0}
                     };

    wgraph.setWeightMatrix(graph);
 
	wgraph.printShortestDistance(0);

  cout << "demonstration of minimum spanning tree: " << endl; 
	//adjacency list representation
	mst.createSpanningGraph();

	mst.prim(0);
	mst.printTreeAndWeight();

}